package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"go-server/router"
)

type Ketan struct {
	Menu string
	Tipe []string
}

type Health struct {
	Status string
}

func Index(w http.ResponseWriter, r *http.Request) {
	k := Ketan{"ketan", []string{"bakar original", "bakar keju"}}
	resp, err := json.Marshal(k)

	if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}

func HealthCheck(w http.ResponseWriter, r *http.Request) {
	s := Health{"good"}
	resp, err := json.Marshal(s)

	if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	w.Write(resp)
}

func main() {
	r := router.Router()
	// fs := http.FileServer(http.Dir("build"))
	// http.Handle("/", fs)
	fmt.Println("Starting server on the port 8080...")

	http.HandleFunc("/health", HealthCheck)
	http.HandleFunc("/", Index)

	log.Fatal(http.ListenAndServe(":8080", r))
}
